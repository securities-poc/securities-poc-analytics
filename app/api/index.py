from flask import jsonify, request

from app.api import bp

@bp.route('/securities', methods=['GET'])
def index():
    return {'symbol': 'MSFT', 'quantity': 500, 'marketPrice': 179.60, 'value' : 89800}