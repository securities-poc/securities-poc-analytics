import pandas as pd
import numpy as np

# https://towardsdatascience.com/pandas-cheat-sheet-4c4eb6802a4b

grades = pd.DataFrame(
    {
        'Math': [80, 89, 93, 66, 84, 85, 74, 64],
        'Science': [94, 76, 88, 78, 88, 92, 60, 85],
        'English': [83, 76, 93, 96, 77, 85, 92, 60],
        'History': [96, 66, 76, 85, 78, 88, 69, 99]
    }
)

# print(grades.head(1))

# print(grades.columns)

# print(grades.info())

print(grades.describe().round(2))

# print(grades.mean())
# print(grades['Math'].mean().round(2))

# print(grades['Math'].median())

# standard deviation
print(grades.std().round(2))

# variance
print(grades.var().round(2))
