import yfinance as yf
import streamlit as st
import datetime
import talib
#import ta
import pandas as pd
import requests

yf.pdr_override()

st.write("""
## Securities Technical Analysis
Indicators:
  - **Moving Average Crossovers**
  - **Bollinger Bands**
  - **MACD's**
  - **Commodity Channel Indexes**
  - **Relative Strength Indexes**
  
""")

st.sidebar.header('Select Stock and Date Range:')

today = datetime.date.today()

def selected_stock_and_date_range():
    ticker = st.sidebar.text_input("Ticker", 'GSK')
    start_date = st.sidebar.text_input("Start Date", '2017-01-01')
    end_date = st.sidebar.text_input("End Date", f'{today}')
    return ticker, start_date, end_date


stock_symbol, stock_start_date, stock_end_date = selected_stock_and_date_range()
    
def get_stock_symbol(stock_symbol):
    url = "http://d.yimg.com/autoc.finance.yahoo.com/autoc?query={}&region=1&lang=en".format(stock_symbol)
    result = requests.get(url).json()
    for x in result['ResultSet']['Result']:
        if x['symbol'] == stock_symbol:
            return x['name']

company_name = get_stock_symbol(stock_symbol.upper()) 
stock_start_date = pd.to_datetime(stock_start_date)
stock_end_date = pd.to_datetime(stock_end_date)
        
# Read stock data    
stock_data = yf.download(stock_symbol, stock_start_date, stock_end_date)

# Adjusted close price
st.header(f"Adjusted Close Price\n {company_name}")
st.line_chart(stock_data['Adj Close'])

# Simple Moving Average - SMA
stock_data['SMA'] = talib.SMA(stock_data['Adj Close'], timeperiod = 20)

# Exponential Moving Average
stock_data['EMA'] = talib.EMA(stock_data['Adj Close'], timeperiod = 20)

# Plot SMA and EMA charts
st.header(f"Simple Moving Average (SMA) vs Exponential Moving Average (EMA)\n {company_name} ")
st.line_chart(stock_data[['Adj Close', 'SMA', 'EMA']])


# Bollinger Bands data points
stock_data['upper_band'], stock_data['middle_band'], stock_data['lower_band'] = talib.BBANDS(stock_data['Adj Close'], timeperiod = 20)


# Plot Bollinger Bands
st.header(f"Bollinger Bands\n {company_name}")
st.line_chart(stock_data[['Adj Close', 'upper_band', 'middle_band','lower_band']])
