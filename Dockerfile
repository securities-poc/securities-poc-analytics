FROM python:3.8.2-alpine

RUN adduser -D appuser

WORKDIR /home/appuser

# Copy source to container file system 
COPY app app

#install dependencies
#RUN pip3 install -r requirements.txt
RUN env/bin/pip3 install --no-cache -r requirements.txt

ENV FLASK_APP securities_poc_analytics.py

RUN chown -r appuser:appuser ./

USER appuser

#EXPOSE 7777

CMD [ "uwsgi", "app.ini" ]