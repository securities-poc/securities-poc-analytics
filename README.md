# Analytics Service

1. Create source folder
   1. mkdir \${app.src.dir}
2. Create and activate virtual environment

   ```bash
      python3 -m venv env
      source env/bin/activate
   ```

3. Install and verify packages - Flask, pickle and uwsgi - in virtual environment:

   ```bash
      ./env/bin/pip3 install flask pickle-mixin uwsgi pandas pandas_datareader numpy yfinance scipy matplotlib tensorflow scikit-learn
      ./env/bin/pip3 list
   ```

4. Build application structure:

   1. create entry point in root directory:start.py

      ```bash
         touch securities_poc_analytics.py
         mkdir ${app.src.dir}/__init__.py
      ```

   2. build api package:

      ```bash
         mkdir ${app.src.dir}/api
         touch ${app.src.dir}/api/__init__.py
      ```

5. Generate a requirements file

   ```bash
        pip3 freeze > requirements.txt
   ```

### TODO

- revisit base Docker image
  - https://pythonspeed.com/articles/alpine-docker-python/
  - https://pythonspeed.com/articles/base-image-python-docker-images/

###### https://mrjbq7.github.io/ta-lib/install.html

##### install Flask and set up application

```bash
mkdir ${project_dir}/api/app
touch ${project_dir}/api/run.py
touch ${project_dir}/api/app/__init__.py
```
